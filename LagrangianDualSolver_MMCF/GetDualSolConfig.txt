# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# - - - - - - - - - - - - GetDualSolConfig.txt- - - - - - - - - - - - - - - -
#
# A txt description of a Configuration that is passed to get_dual_solution()
# of the second Solver (LagrangianDualSolver) in test.cpp
#
#   Antonio Frangioni
#
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# get the dual solution of the last 20 components only, passing nullptr as
# the Configuration of their get_dual_solution()
SimpleConfiguration<std::vector<std::pair<int,int>>>
20
20	-1
21	-1
22	-1
23	-1
24	-1
25	-1
26	-1
27	-1
28	-1
29	-1
30	-1
31	-1
32	-1
33	-1
34	-1
35	-1
36	-1
37	-1
38	-1
39	-1

# - - - - - - - - - - - end GetDualSolConfig.txt- - - - - - - - - - - - - - -
