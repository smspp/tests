# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# - - - - - - - - - - - - - - - - BSPar2.txt- - - - - - - - - - - - - - - - -
#
# A txt description of a BlockSolverConfig for a
# CapacitatedFacilityLocationBlock to be solved by a :MILPSolver
#
#   Antonio Frangioni
#
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

BlockSolverConfig     # exact type of the Configuration object

1  # the BlockSolverConfig is a "differential" one

1  # number of (the names of) Solver in this BlockSolverConfig
# now all the names of the Solver - - - - - - - - - - - - - - - - - - - - - -
CPXMILPSolver    # name of Solver
#GRBMILPSolver   # name of Solver
#SCIPMILPSolver  # name of Solver
#HiGHSMILPSolver # name of Solver

1  # number of ComputeConfig in this BlockSolverConfig

# now all the ComputeConfig
# 1st ComputeConfig - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# ComputeConfig of the :MILPSolver

ComputeConfig # exact type of the ComputeConfig object

1  # f_diff == 0 ==> all non-provided parameters are set to the default value
   # f_diff == 1 ==> all non-provided parameters are not changed

9  # number of integer parameters

# now all the integer parameters
intLogVerb	       	       	       	0	     # log verbosity of the MILPSolver
intRelaxIntVars	       	       	    1	     # nonzero if the continuous relaxation is solved
intCutSepPar	       	       	    0	     # separate user cuts at the root node & sub-nodes

## All specific CPLEX parameter (uncomment based on Solver)
CPXPARAM_Threads	       	       	1	     # avoid parallel
CPXPARAM_MIP_Display	       	    3	     # more verbose log
CPXPARAM_ScreenOutput	       	    0	     # output on screen
CPXPARAM_MIP_Limits_Nodes	       	0	     # only do root node
CPXPARAM_MIP_Limits_EachCutLimit	0	     # disable standard cuts
CPXPARAM_MIP_Strategy_HeuristicFreq	0	     # disable heuristic
#CPXPARAM_LPMethod	      	        3	     # 0 = auto (default), 1 = primal, 2 = dual, 3 = network, 4 = barrier, 5 = sifting, 6 = concurrent

## All specific GUROBI parameter (uncomment based on Solver)
#Method 4  # -1 = auto (default), 0 = primal, 1 = dual, 2 = barrier, 3 = barrier + crossover, 4 = network

1 # number of double parameters

# now all the double parameters
dblMaxTime 60   # max time: 1 minute

0 # number of string parameters

# now all the string parameters
#strOutputFile	cfl2.lp

0 # number of vector-of-int parameters

# now all the vector-of-int parameters
# [none]

0 # number of vector-of-double parameters

# now all the vector-of-double parameters
# [none]

0 # number of vector-of-string parameters

# now all the vector-of-string parameters
# [none]

# pointer to the "extra" Configuration
* # [none]

# end of 1st ComputeConfig- - - - - - - - - - - - - - - - - - - - - - - - - -

# end of BlockSolverConfig- - - - - - - - - - - - - - - - - - - - - - - - - -

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# - - - - - - - - - - - - - - - - END BSPar2.txt- - - - - - - - - - - - - - -
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
